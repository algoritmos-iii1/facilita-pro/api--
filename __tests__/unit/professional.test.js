const Professional = require("../../src/models/Professional");
const Profession = require("../../src/models/Profession");
const bcrypt = require("bcryptjs");

describe("Professional unit tests", () => {
	var profession = {}
  beforeAll(async () => {
    profession = await Profession.create({
      name: "Eletricista",
    });
  });

  test("Deve encriptar o password", async () => {
    const prof = await Professional.create({
      profession_id: profession.id,
      name: "Silas Costa",
      slug: "silas-costa",
      birth: "1992/11/07",
      cpf: "6858-8420",
      phone: "335444",
      email: "SilasCosta.Martins42@live.com",
      password: "silas-costa123",
    });

    const compare = await bcrypt.compare(prof.password, prof.passwordHash);

    expect(compare).toBe(true);
  });
});
