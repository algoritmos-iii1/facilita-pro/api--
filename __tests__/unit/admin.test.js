const Admin = require("../../src/models/Admin");
const bcrypt = require("bcryptjs");

describe("Admin unit tests", () => {
  test("Deve encriptar o password", async () => {
    const admin = await Admin.create({
      name: "Morgana Nogueira",
      slug: "morgana-nogueira",
      birth: "1972/09/17",
      cpf: "5717-1878",
      phone: "354423444",
      email: "MorganaNogueira_Santos@yahoo.com",
      password: "morgana-nogueira123",
    });

    const compare = await bcrypt.compare(admin.password, admin.passwordHash);

    expect(compare).toBe(true);
  });
});
