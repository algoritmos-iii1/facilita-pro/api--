const request = require("supertest");
const app = require("../../App");
const Client = require("../../src/models/Client");
const Professional = require("../../src/models/Professional");
const Profession = require("../../src/models/Profession");
const Admin = require("../../src/models/Admin");

var client = {};

describe("Client login tests", () => {
  beforeAll(async () => {
    client = await Client.create({
      name: "Sra. Eduardo Carvalho",
      slug: "sra.-eduardo-carvalho",
      birth: "1972/11/07",
      cpf: "99991111111222222",
      phone: "354423444",
      email: "Sra.EduardoCarvalho_Reis@gmail.com",
      password: "sra.-eduardo-carvalho123",
      recommendation: "facebook",
    });
  });

  test("Deve ser possível logar cliente com credenciais corretas", async () => {
    await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" })
      .expect(200);
  });

  test("Não deve ser possível logar cliente com password incorreto", async () => {
    client.password = '42342343'
    await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" })
      .expect(401);
    client.password = "sra.-eduardo-carvalho123";
  });

  test("Não deve ser possível logar cliente com email incorreto", async () => {
    client.email = 'qiheasnd'
    await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" })
      .expect(401);
    client.email = "Sra.EduardoCarvalho_Reis@gmail.com";
  });

  test("Não deve ser possível logar cliente sem email", async () => {
    //let client = await cliFactory.create('Client', {})
    client.email = null
    await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" })
      .expect(404);
    client.email = "Sra.EduardoCarvalho_Reis@gmail.com";
  });

  test("Não deve ser possível logar cliente sem password", async () => {
    //let client = await cliFactory.create('Client', {})
    client.password = null
    await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" })
      .expect(404);
    client.password = "sra.-eduardo-carvalho123";
  });

  test("Deve retornar um token quando cliente logado", async () => {
    // let client = await cliFactory.create('Client', {})
    const response = await request(app)
      .get("/login")
      .auth(client.email, client.password, { type: "auto" });

    expect(response.body).toHaveProperty("token");
  });
});

var prof = {};
var profession = {};

describe("Profissional login tests", () => {
  beforeAll(async () => {
    profession = await Profession.create({
      name: "Programador",
    });

    prof = await Professional.create({
      profession_id: profession.id,
      name: "Célia Moreira",
      slug: "celia-moreira",
      birth: "1992/11/07",
      cpf: "231",
      phone: "335444",
      email: "CliaMoreira_Martins93@live.com",
      password: "celia-moreira123",
    });
  });

  test("Deve ser possível logar profissional com credenciais corretas", async () => {
    await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" })
      .expect(200);
  });

  test("Não deve ser possível logar profissional com password incorreto", async () => {
    prof.password = "23423";
    await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" })
      .expect(401);
    prof.password = "celia-moreira123";
  });

  test("Não deve ser possível logar profissional com email incorreto", async () => {
    prof.email = "anything";
    await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" })
      .expect(401);
    prof.email = "CliaMoreira_Martins93@live.com";
  });

  test("Não deve ser possível logar profissional sem email", async () => {
    prof.email = null;
    await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" })
      .expect(404);
    prof.email = "CliaMoreira_Martins93@live.com";
  });

  test("Não deve ser possível logar profissional sem password", async () => {
    prof.password = null;
    await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" })
      .expect(404);
    prof.password = "celia-moreira123";
  });

  test("Deve retornar um token quando profissional logado", async () => {
    const response = await request(app)
      .get("/login")
      .auth(prof.email, prof.password, { type: "auto" });

    expect(response.body).toHaveProperty("token");
  });
});

var admin = {};

describe("Admin login tests", () => {
  beforeAll(async () => {
    admin = await Admin.create({
      name: "Administrador",
      slug: "administrador",
      birth: "1972/09/17",
      cpf: "246776988",
      phone: "354423444",
      email: "admin1@gmail.com",
      password: "admin123",
    });
  });

  test("Deve ser possível logar administrador com credenciais corretas", async () => {
    await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" })
      .expect(200);
  });

  test("Não deve ser possível logar administrador com password incorreto", async () => {
    admin.password = "23423";
    await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" })
      .expect(401);
    admin.password = "admin123";
  });

  test("Não deve ser possível logar administrador com email incorreto", async () => {
    admin.email = "anything";
    await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" })
      .expect(401);
    admin.email = "admin1@gmail.com";
  });

  test("Não deve ser possível logar administrador sem email", async () => {
    admin.email = null;
    await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" })
      .expect(404);
    admin.email = "admin1@gmail.com";
  });

  test("Não deve ser possível logar administrador sem password", async () => {
    admin.password = null;
    await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" })
      .expect(404);
    admin.password = "admin123";
  });

  test("Deve retornar um token quando administrador logado", async () => {
    const response = await request(app)
      .get("/login")
      .auth(admin.email, admin.password, { type: "auto" });

    expect(response.body).toHaveProperty("token");
  });
});
