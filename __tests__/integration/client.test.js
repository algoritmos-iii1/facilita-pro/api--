const request = require("supertest");
const app = require("../../App");
const Client = require("../../src/models/Client");
const Admin = require("../../src/models/Admin");

var client = {
  name: "Natália Batista",
  birth: "1972/11/07",
  cpf: "4097-2104",
  phone: "354423444",
  email: "NatliaBatista_Melo84@bol.com.br",
  password: "natalia-batista123",
  recommendation: "Google",
};

describe("POST/clients", () => {
  test("Deve ser possível criar cliente com todas as informações corretas", async () => {
    await request(app).post("/clients").send(client).expect(201);
  });

  test("Não deve ser possível criar cliente sem nome", async () => {
    client.name = null;
    await request(app).post("/clients").send(client).expect(400);
    client.name = "Natália Batista";
  });

  test("Não deve ser possível criar cliente sem data de nascimento", async () => {
    client.birth = null;
    await request(app).post("/clients").send(client).expect(400);
    client.birth = "1972/11/07";
  });

  test("Não deve ser possível criar cliente sem cpf", async () => {
    client.cpf = null;
    await request(app).post("/clients").send(client).expect(400);
    client.cpf = "4097-2104";
  });

  test("Não deve ser possível criar cliente sem telefone", async () => {
    client.phone = null;
    await request(app).post("/clients").send(client).expect(400);
    client.phone = "354423444";
  });

  test("Não deve ser possível criar cliente sem email", async () => {
    client.email = null;
    await request(app).post("/clients").send(client).expect(400);
    client.email = "NatliaBatista_Melo84@bol.com.br";
  });

  test("Não deve ser possível criar cliente sem password", async () => {
    client.password = null;
    await request(app).post("/clients").send(client).expect(400);
    client.password = "natalia-batista123";
  });

  test("Não deve ser possível criar cliente sem recommendation", async () => {
    client.recommendation = null;
    await request(app).post("/clients").send(client).expect(400);
    client.recommendation = "Google";
  });
});

var auth;
var token;
var newClient;
data = {
  name: "Yango Reis",
  slug: "yango-reis",
  birth: "1972/11/07",
  cpf: "2385-1214",
  phone: "354423444",
  email: "YangoReis.Moraes@yahoo.com",
  password: "yango-reis123",
  recommendation: "Televisão",
};

describe("PUT/clients", () => {
  beforeAll(async () => {
    auth = await Client.create({
      name: "Frederico Batista",
      slug: "frederico-batista",
      birth: "1972/11/07",
      cpf: "3015-4367",
      phone: "354423444",
      email: "FredericoBatista_Santos73@live.com",
      password: "frederico-batista123",
      recommendation: "Televisão",
    });

    newClient = await Client.create({
      name: data.name,
      slug: data.slug,
      birth: data.birth,
      cpf: data.cpf,
      phone: data.phone,
      email: data.email,
      password: data.password,
      recommendation: data.recommendation,
    });

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível editar o nome do cliente com todas as informações corretas e token válido", async () => {
    data.name = "Maxwell Laner";
    const response = await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200);

    expect(response.body.name).toBe("Maxwell Laner");
    expect(response.body.slug).toBe("maxwell-laner");
  });

  test("Não deve ser possível editar o nome do cliente sem enviar um nome", async () => {
    data.name = null;
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(400);
    data.name = "Maxwell Laner";
  });

  test("Deve ser possível editar a data de nascimento com todas as informações corretas e token válido", async () => {
    data.birth = "2020/02/13";
    const response = await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200);

    //expect(response.body.birth).toBe('2020/02/13') a data do responde vem "2020-02-13T03:00:00.000Z", mas está correto
  });

  test("Não deve ser possível editar a data de nascimento sem enviar uma data de nascimento", async () => {
    data.birth = null;
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(400);
    data.birth = "2020/02/13";
    //expect(response.body.birth).toBe('2020/02/13') a data do responde vem "2020-02-13T03:00:00.000Z", mas está correto
  });

  test("Deve ser possível editar o phone com todas as informações corretas e token válido", async () => {
    data.phone = "34653456456";
    const response = await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200);

    expect(response.body.phone).toBe("34653456456");
  });

  test("Não deve ser possível editar o phone sem enviar um phone", async () => {
    data.phone = null;
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(400);

    data.phone = "34653456456";
  });

  test("Deve ser possível editar o cpf com todas as informações corretas e token válido", async () => {
    data.cpf = "1234567890";
    const response = await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200);

    expect(response.body.cpf).toBe("1234567890");
  });

  test("Não deve ser possível editar o cpf sem enviar um cpf", async () => {
    data.cpf = null;
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(400);

    data.cpf = "1234567890";
  });

  test("Deve ser possível editar o email com todas as informações corretas e token válido", async () => {
    data.email = "laner@gmail.com";
    const response = await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200);

    expect(response.body.email).toBe("laner@gmail.com");
  });

  test("Não deve ser possível editar o email sem enviar um email", async () => {
    data.email = null;
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(400);

    data.email = "laner@gmail.com";
  });

  test("Não deve ser possível editar registros sem token", async () => {
    data.nome = "Maxwell R. Laner";
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer `)
      .send(data)
      .expect(401);
  });

  test("Não deve ser possível editar registros com token inválido", async () => {
    data.nome = "Maxwell R. Laner";
    await request(app)
      .put(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer lnfSDF%@@45W@#¨Vscf**(A)afrhj@`)
      .send(data)
      .expect(401);
  });
});

describe("GET/clients", () => {
  beforeAll(async () => {
    auth = await Admin.create({
      name: "Administrador5",
      slug: "administrador5",
      birth: "1972/09/17",
      cpf: "269999888880",
      phone: "354423444",
      email: "admin5@gmail.com",
      password: "administrador123",
    });

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível buscar todos os clientes com token válido", async () => {
    await request(app)
      .get("/clients")
      .set("Authorization", `Bearer ${token}`)
      .expect(200);
  });

  test("Não deve ser possível buscar todos os clientes sem token", async () => {
    await request(app)
      .get("/clients")
      .set("Authorization", `Bearer `)
      .expect(401);
  });

  test("Não deve ser possível buscar todos os clientes com token inválido", async () => {
    await request(app)
      .get("/clients")
      .set("Authorization", `Bearer 423534jkasbco45/#%@#$%@sdmfnmlm423`)
      .expect(401);
  });
});

describe("GET/clients/:id", () => {
  beforeAll(async () => {
    auth = await Client.create({
      name: "Srta. Gúbio Souza",
      slug: "srta.-gubio-souza",
      birth: "1972/11/07",
      cpf: "2454-3877",
      phone: "354423444",
      email: "Srta.GbioSouza_Costa41@bol.com.br",
      password: "srta.-gubio-souza123",
      recommendation: "Televisão",
    });

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível buscar um cliente pelo id com token válido", async () => {
    const response = await request(app)
      .get(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200);

    expect(response.body.name).toBe("Maxwell Laner");
  });

  test("Não deve ser possível buscar um cliente pelo id sem token", async () => {
    await request(app)
      .get(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer `)
      .expect(401);
  });

  test("Não deve ser possível buscar um cliente pelo id com token inválido", async () => {
    await request(app)
      .get(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer 9kndf#$%¨6gdg4567&FEsdfdfghyu7qDFG#%`)
      .expect(401);
  });
});

describe("DELETE/clients/:id", () => {
  beforeAll(async () => {
    auth = await Client.create({
      name: "Qualquer nome",
      slug: "qualquer-nome",
      birth: "1972/11/07",
      cpf: "3423455677898789",
      phone: "354423444",
      email: "umemailqualquer@live.com",
      password: "123456",
      recommendation: "Televisão",
    });

    newClient = await Client.create({
      name: "cliente deletado",
      slug: "data.slug",
      birth: "1999/06/05",
      cpf: "2345346564567687",
      phone: "4255346456",
      email: "data.email@gmail.com",
      password: "data.password",
      recommendation: "facebook",
    });

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível deletar um cliente com id e token válidos", async () => {
    await request(app)
      .delete(`/clients/${newClient.id}`)
      .set("Authorization", `Bearear ${token}`)
      .expect(204);
  });

  test("Não deve ser possível deletar um cliente com id inexistente", async () => {
    await request(app)
      .delete(`/clients/${4567678686789}`)
      .set("Authorization", `Bearer ${token}`)
      .expect(404);
  });

  test("Não deve ser possível deletar um cliente com token inválido", async () => {
    await request(app)
      .delete(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer 398472g#$%#$gsdnjfon6@%$%¨FBDVf`)
      .expect(401);
  });

  test("Não deve ser possível deletar um cliente sem enviar um token", async () => {
    await request(app)
      .delete(`/clients/${newClient.id}`)
      .set("Authorization", `Bearer `)
      .expect(401);
  });
  
});
