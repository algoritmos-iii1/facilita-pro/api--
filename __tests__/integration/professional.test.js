const request = require("supertest");
const app = require("../../App");
const Profession = require("../../src/models/Profession");
const Admin = require("../../src/models/Admin");

var professional = {
  profession_id: "",
  name: "Vitória Martins",
  birth: "1992/11/07",
  cpf: "2319900999",
  phone: "335444",
  email: "VitriaMartins.Souza13@hotmail.com",
  password: "vitoria-martins123",
  description: "",
  file: "",
};

var profession = {};
var auth = {};
var token;

describe("Professional integration tests", () => {
  beforeAll(async () => {
    auth = await Admin.create({
      name: "Administrador3",
      slug: "administrador3",
      birth: "1972/09/17",
      cpf: "24000888888",
      phone: "354423444",
      email: "admin3@gmail.com",
      password: "administrador3123",
    });
    profession = await Profession.create({
      name: "Encanador",
    });
    professional.profession_id = profession.id;

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível criar profissional com todas as informações corretas", async () => {
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(201);
  });

  test("Não deve ser possível criar profissional sem o id da profissão", async () => {
    professional.profession_id = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.profession_id = profession.id;
  });

  test("Não deve ser possível criar profissional sem nome", async () => {
    professional.name = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.name = "Vitória Martins";
  });

  test("Não deve ser possível criar profissional sem data de nascimento", async () => {
    professional.birth = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.birth = "1992/11/07";
  });

  test("Não deve ser possível criar profissional sem cpf", async () => {
    professional.cpf = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.cpf = "2319900999";
  });

  test("Não deve ser possível criar profissional sem phone", async () => {
    professional.phone = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.phone = "335444";
  });

  test("Não deve ser possível criar profissional sem email", async () => {
    professional.email = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.email = "VitriaMartins.Souza13@hotmail.com";
  });

  test("Não deve ser possível criar profissional sem password", async () => {
    professional.password = null;
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer ${token}`)
      .send(professional)
      .expect(400);
    professional.password = "vitoria-martins123";
  });

  test("Não deve ser possível criar profissional sem token", async () => {
    await request(app)
      .post("/professionals")
      .set("Authorization", `Bearer `)
      .send(professional)
      .expect(401);
  });

  test("Não deve ser possível criar profissional com token inválido", async () => {
    await request(app)
      .post("/professionals")
      .set(
        "Authorization",
        "Bearer 3jhj239@#JF@#JKVCZK32cjiopz56@*(@fas#@45vSF"
      )
      .send(professional)
      .expect(401);
  });
});
