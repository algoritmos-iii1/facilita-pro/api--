const request = require("supertest");
const app = require("../../App");
const Admin = require("../../src/models/Admin");

var admin = {
  name: "Elísio Reis Neto",
  birth: "1972/09/17",
  cpf: "0990-9773",
  phone: "354423444",
  email: "ElsioReisNeto39@live.com",
  password: "elisio-reis-neto123",
};

var auth = {};
var token;

describe("Admin integration tests", () => {
  beforeAll(async () => {
    auth = await Admin.create({
      name: "Administrador2",
      slug: "administrador2",
      birth: "1972/09/17",
      cpf: "246776922222288",
      phone: "354423444",
      email: "admin2@gmail.com",
      password: "administrador2123",
    });

    const login = await request(app)
      .get("/login")
      .auth(auth.email, auth.password, { type: "auto" })
      .expect(200);

    token = login.body.token;
  });

  test("Deve ser possível criar um admin com todas as informações corretas", async () => {
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(201);
  });

  test("Não deve ser possível crair admin sem nome", async () => {
    admin.name = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.name = "Administrador2";
  });
  
  test("Não deve ser possível crair admin sem data de nascimento", async () => {
    admin.birth = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.birth = "1972/09/17"
  });

  test("Não deve ser possível crair admin sem cpf", async () => {
    admin.cpf = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.cpf = "0990-9773"
  });

  test("Não deve ser possível crair admin sem phone", async () => {
    admin.phone = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.phone = "354423444"
  });

  test("Não deve ser possível crair admin sem email", async () => {
    admin.email = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.email = "admin2@gmail.com"
  });

  test("Não deve ser possível crair admin sem password", async () => {
    admin.password = null;
    await request(app)
      .post("/admins")
      .set("Authorization", `Bearer ${token}`)
      .send(admin)
      .expect(400);
    admin.password = "administrador2123"
  });
});
