const Client = require("../models/Client");
const Professional = require("../models/Professional");
const Admin = require("../models/Admin");

class SessionController {
  async login(req, res) {
    const headerAuth = req.headers.authorization;
    if (!headerAuth) {
      return res.status(400).json({ message: "Missing email or password" });
    }
    const [, hash] = req.headers.authorization.split(" ");

    const [email, password] = Buffer.from(hash, "base64").toString().split(":");
    //const { email, password } = req.body

    const client = await Client.findOne({ where: { email: email } });

    if (client) {
      if (!(await client.checkPassword(password))) {
        return res.status(401).json({ message: "Invalid Password" });
      }
      return res.json({
        client,
        token: client.generateToken(),
      });
    }
    const professional = await Professional.findOne({
      where: { email: email },
    });
    if (professional) {
      if (!(await professional.checkPassword(password))) {
        return res.status(401).json({ message: "Invalid Password" });
      }
      return res.json({
        professional,
        token: professional.generateToken(),
      });
    }
    const admin = await Admin.findOne({
      where: { email: email },
    });
    if (admin) {
      if (!(await admin.checkPassword(password))) {
        return res.status(401).json({ message: "Invalid Password" });
      }
      return res.json({
        admin,
        token: admin.generateToken(),
      });
    }
    return res.status(401).json({ message: "User not found with this e-mail" });
  }

  async logout(req, res) {
    // Excluir o token no lado do cliente?
  }
}

module.exports = new SessionController();
