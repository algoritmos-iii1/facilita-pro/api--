const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Profession = require("../models/Profession");
const Address = require("../models/Address");
const ClientReviews = require("../models/ClientReviews");

class Professional extends Model {}
Professional.init(
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    slug: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    birth: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    cpf: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    description: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.VIRTUAL,
      allowNull: false,
    },
    passwordHash: {
      type: Sequelize.STRING,
    },
    photoUrl: {
      type: Sequelize.STRING,
    },
  },
  {
    sequelize,
    paranoid: true,
    deletedAt: "deletedAt",
    modelName: "professional",
    hooks: {
      beforeSave: async (professional) => {
        if (professional.password) {
          professional.passwordHash = await bcrypt.hash(
            professional.password,
            8
          );
        }
      },
    },
  }
);

Professional.prototype.checkPassword = function (password) {
  return bcrypt.compare(password, this.passwordHash);
};

Professional.prototype.generateToken = function () {
  return jwt.sign({ cpf: this.cpf }, process.env.APP_SECRET, {
    expiresIn: "24h",
  });
};

Professional.belongsTo(Profession, {
  foreignKey: "profession_id",
  as: "profession",
});
Profession.hasMany(Professional, {
  foreignKey: "profession_id",
  as: "professional",
});

Professional.hasMany(Address, { foreignKey: "professional_id", as: "address" });
Address.belongsTo(Professional, {
  foreignKey: "professional_id",
  as: "professional",
});

Professional.hasMany(ClientReviews, {
  foreignKey: "professional_id",
  as: "assessments",
});
ClientReviews.belongsTo(Professional, {
  foreignKey: "professional_id",
  as: "professional",
});

module.exports = Professional;
