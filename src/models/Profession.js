const Sequelize = require('sequelize')
const Model = Sequelize.Model
const sequelize = require('../connection')

class Profession extends Model {}
Profession.init({
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  }
}, {
  sequelize,
  modelName: 'profession'
})

module.exports = Profession;