const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const Solicitation = require("./Solicitation");
const Professional = require("./Professional");

class Budget extends Model {}
Budget.init(
  {
    professional_id: {
      type: Sequelize.INTEGER,
      onDelete: "CASCADE",
      references: {
        model: "Professionals",
        key: "id",
      },
    },
    solicitation_id: {
      type: Sequelize.INTEGER,
      references: {
        model: "Solicitations",
        key: "id",
      },
    },
    value: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    tax: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    total: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: "budget",
  }
);

Professional.hasMany(Budget, {
  foreignKey: "professional_id",
  as: "budgets",
});
Solicitation.hasMany(Budget, {
  foreignKey: "solicitation_id",
  as: "budgets",
});
Budget.belongsTo(Professional, {
  foreignKey: "professional_id",
  as: "professional",
});
Budget.belongsTo(Solicitation, {
  foreignKey: "solicitation_id",
  as: "solicitation",
});

Solicitation.addScope("defaultScope", {
  include: {
    model: Budget,
    as: "budgets",
  },
});

module.exports = Budget;
