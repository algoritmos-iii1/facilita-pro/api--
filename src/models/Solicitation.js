const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../connection");
const Client = require("./Client");
const Service = require("./Service");
const Address = require("../models/Address");

class Solicitation extends Model {}
Solicitation.init(
  {
    client_id: {
      type: Sequelize.INTEGER,
      onDelete: "CASCADE",
      references: {
        model: "Clients",
        key: "id",
      },
    },
    service_id: {
      type: Sequelize.INTEGER,
      references: {
        model: "Services",
        key: "id",
      },
    },
    address_id: {
      type: Sequelize.INTEGER,
      references: {
        model: "Address",
        key: "id",
      },
    },
    initialDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    finallyDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    photoUrl: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    rated: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: "solicitation",
  }
);

Client.hasMany(Solicitation, { foreignKey: "client_id", as: "solicitations" });
Service.hasMany(Solicitation, {
  foreignKey: "service_id",
  as: "solicitations",
});
Solicitation.belongsTo(Client, { foreignKey: "client_id", as: "clients" });
Solicitation.belongsTo(Service, { foreignKey: "service_id", as: "services" });
Address.hasMany(Solicitation, { foreignKey: "address_id", as: "solicitation" });
Solicitation.belongsTo(Address, { foreignKey: "address_id", as: "address" });

module.exports = Solicitation;
