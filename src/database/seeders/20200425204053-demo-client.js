"use strict";

const faker = require("faker");
const slugify = require("slugify");
const bcrypt = require("bcryptjs");
faker.locale = "pt_BR";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let data = [];
    let amount = 100
    
    while(amount--) {
      let date = new Date()
      
      const name = faker.name.findName()
      const slug = slugify(name, {
        lower: true,
      });
      const birth = faker.date.past(50, 2002)
      const cpf = faker.random.number()
      const phone = faker.phone.phoneNumber()
      const email = faker.internet.email(name)
      const recommendation = "facebook"
      const passwordHash = await bcrypt.hash(slug + "123", 8)
      const createdAt = date
      const updatedAt = date
      const deletedAt = 0
      
      let client = {
        name,
        slug,
        birth,
        cpf,
        phone,
        email,
        recommendation,
        passwordHash,
        createdAt,
        updatedAt,
        deletedAt
      }
       
      data.push(client)
    }
    return queryInterface.bulkInsert("clients", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("clients", null, {});
  },
};
