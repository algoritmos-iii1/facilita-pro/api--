"use strict";

const faker = require("faker");
const slugify = require("slugify");
const bcrypt = require("bcryptjs");
faker.locale = "pt_BR";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }
    let data = [];
    let amount = 100;

    while (amount--) {
      let date = new Date();
      const profession_id = random(1, 5);
      const name = faker.name.findName();
      const slug = slugify(name, {
        lower: true,
      });
      const birth = faker.date.past(50, 2002);
      const cpf = faker.random.number();
      const phone = faker.phone.phoneNumber();
      const email = faker.internet.email(name);
      const passwordHash = await bcrypt.hash(slug + "123", 8);
      const createdAt = date;
      const updatedAt = date;
      const deletedAt = 0;

      let professional = {
        profession_id,
        name,
        slug,
        birth,
        cpf,
        phone,
        email,
        passwordHash,
        createdAt,
        updatedAt,
        deletedAt,
      };
      data.push(professional);
    }
    return queryInterface.bulkInsert("professionals", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("professionals", null, {});
  },
};
