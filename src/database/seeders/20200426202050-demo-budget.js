"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }
    let data = [];
    let amount = 100;
    let date = new Date();

    while (amount--) {
      const professional_id = random(1, 100);
      const solicitation_id = random(1, 100);
      const value = random(100, 5000);
      const tax = random(1, 20);
      const status = "aberto";
      const total = (tax * value) / 100 + parseFloat(value);
      const updatedAt = date;
      const createdAt = date;

      let budget = {
        professional_id,
        solicitation_id,
        value,
        tax,
        status,
        total,
        updatedAt,
        createdAt,
      };
      data.push(budget);
    }

    return queryInterface.bulkInsert("budgets", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("budgets", null, {});
  },
};
