"use strict";

const faker = require("faker");
faker.locale = "pt_BR";

module.exports = {
  up: (queryInterface, Sequelize) => {
    function random(min, max) {
      return min + Math.floor((max - min) * Math.random());
    }
    let data = []
    let amount = 100
    let date = new Date()
    while(amount--) {
      const street = faker.address.streetAddress()
      const number = random(1, 500)
      const complement = "apartamento " + random(1, 400)
      const neighborhood = faker.address.state()
      const city = faker.address.city()
      const zipcode = faker.address.zipCode()
      const createdAt = date
      const updatedAt = date

      if (amount % 2 == 0) {
        const client_id = random(1, 100)
        let address = {
          client_id,
          street,
          number,
          complement,
          neighborhood,
          city,
          zipcode,
          createdAt,
          updatedAt
        }
        data.push(address)
      } else {
        const professional_id = random(1, 100)
        let address = {
          professional_id,
          street,
          number,
          complement,
          neighborhood,
          city,
          zipcode,
          createdAt,
          updatedAt
        }
        data.push(address)
      } 
    }
    return queryInterface.bulkInsert("addresses", data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("addresses", null, {});
  },
};
