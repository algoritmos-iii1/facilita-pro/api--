const Client = require("../models/Client");
const Solicitation = require("../models/Solicitation");
const Budget = require("../models/Budget");
const mail = require("../mail/util/mail.util");
const slugify = require("slugify");
class ClientController {
  async store(req, res) {
    try {
      const client = req.body;
      const existingEmail = await Client.findOne({
        where: { email: client.email },
      });
      if (existingEmail) {
        return res
          .status(400)
          .json({ message: "Existing customer with this email" });
      }
      const existingCPF = await Client.findOne({
        where: { cpf: client.cpf },
      });
      if (existingCPF) {
        return res
          .status(400)
          .json({ message: "Existing customer with this cpf" });
      }
      try {
        let slug = slugify(client.name, {
          lower: true,
        });
        const newClient = await Client.create({
          name: client.name,
          slug: slug,
          birth: client.birth,
          cpf: client.cpf,
          phone: client.phone,
          email: client.email,
          password: client.password,
          recommendation: client.recommendation,
        });
        return res.status(201).send(newClient);
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
      try {
        const subject = `<FacilitaPRO> Seja Bem-Vindo a FacilitaPRO!`;
        const html =
          `Seja bem vindo a nossa plataforma ${client.name}` +
          (await mail(client.email, subject, html));
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const clients = await Client.findAll({
        include: [
          {
            model: Solicitation,
            as: "solicitations",
            include: [{ model: Budget, as: "budgets" }],
          },
        ],
      });
      if (!clients) {
        return res
          .status(404)
          .json({ message: "There are no records in the table clients" });
      } else {
        return res.status(200).send(clients);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error" });
    }
  }

  async paginate(req, res) {
    try {
      const limit = parseInt(req.query.limit);
      const page = parseInt(req.query.page) * limit;

      const clients = await Client.findAndCountAll({
        order: [["id", "ASC"]],
        include: [
          {
            all: true,
          },
        ],
        limit: limit,
        offset: page,
      });
      let previous = {};
      if (parseInt(req.query.page) <= 0) {
        previous = {
          page: 0,
          limit: limit,
        };
      } else {
        previous = {
          page: parseInt(req.query.page) - 1,
          limit: limit,
        };
      }

      const count = await Client.count();
      let next = {};

      const totalPages = parseInt(count / limit);

      if (parseInt(req.query.page) <= totalPages) {
        next = {
          page: parseInt(req.query.page) + 1,
          limit: limit,
        };
      } else {
        return res.status(400).json({ message: `total pages: ${totalPages}` });
      }

      const object = {
        totalPages: totalPages,
        previous: previous,
        next: next,
        clients: clients,
      };
      if (clients) {
        return res.status(200).send(object);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table clients" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    const client = await Client.findOne({
      where: { id: req.params.id },
      include: [
        {
          all: true,
        },
      ],
    });
    if (client) {
      return res.status(200).send(client);
    } else {
      return res.status(404).json({ message: "Client not found" });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const client = await Client.findOne({
        where: { id: req.params.id },
      });
      if (!client) {
        return res.status(404).json({ message: "Client not found" });
      }
      try {
        let slug = slugify(data.name, {
          lower: true,
        });
        await client
          .update({
            name: data.name,
            slug: slug,
            birth: data.birth,
            cpf: data.cpf,
            phone: data.phone,
            email: data.email,
          })
          .then((updateClient) => res.status(200).send(updateClient));
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const client = await Client.findByPk(req.params.id);
      if (!client) {
        return res.status(404).json({ message: "Client not found" });
      }
      await Client.destroy({
        where: { id: req.params.id },
      }).then(res.status(204).json({ message: "Client deleted successfully" }));
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async restore(req, res) {
    try {
      const client = await Client.findOne({
        where: { id: req.params.id },
        paranoid: false,
      });
      if (!client) {
        return res.status(404).json({ message: "Client not found" });
      }
      await client.restore();
      return res.status(200).json({ message: "Restored client" });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ClientController();
