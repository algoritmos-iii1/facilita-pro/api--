const Professional = require("../models/Professional");
const slugify = require("slugify");
const mail = require("../mail/util/mail.util");

class ProfessionalController {
  async store(req, res) {
    try {
      const { description = "", ...prof } = req.body;
      const { location: url = "" } = req.file;

      const existingEmail = await Professional.findOne({
        where: { email: prof.email },
      });
      if (existingEmail) {
        return res
          .status(400)
          .json({ message: "Existing professional with this email" });
      }
      const existingCPF = await Professional.findOne({
        where: { cpf: prof.cpf },
      });
      if (existingCPF) {
        return res
          .status(400)
          .json({ message: "Existing professional with this cpf" });
      }
      try {
        let slug = slugify(prof.name, {
          lower: true,
        });
        await Professional.create({
          profession_id: prof.profession_id,
          name: prof.name,
          slug: slug,
          birth: prof.birth,
          cpf: prof.cpf,
          phone: prof.phone,
          email: prof.email,
          password: prof.password,
          description: description,
          photoUrl: url,
        }).then((newProf) => res.status(201).send(newProf));
      } catch (err) {
        return res.status(400).json({ message: "Missing data", err });
      }
      try {
        const subject = `<FacilitaPRO> Seja Bem-Vindo a FacilitaPRO!`;
        const html =
          `Seja bem vindo a nossa plataforma ${professional.name}` +
          (await mail(professional.email, subject, html));
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const professionals = await Professional.findAll({
        order: [["id", "ASC"]],
        include: [
          {
            all: true,
          },
        ],
      });
      if (!professionals) {
        return res
          .status(404)
          .json({ message: "There are no records in the table professionals" });
      } else {
        return res.status(200).send(professionals);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error" });
    }
  }

  async paginate(req, res) {
    try {
      const limit = parseInt(req.query.limit);
      const page = parseInt(req.query.page) * limit;

      const professionals = await Professional.findAndCountAll({
        order: [["id", "ASC"]],
        include: [
          {
            all: true,
          },
        ],
        limit: limit,
        offset: page,
      });

      let previous = {};
      if (parseInt(req.query.page) <= 0) {
        previous = {
          page: 0,
          limit: limit,
        };
      } else {
        previous = {
          page: parseInt(req.query.page) - 1,
          limit: limit,
        };
      }

      const count = await Professional.count();
      let next = {};

      const totalPages = parseInt(count / limit);

      if (parseInt(req.query.page) <= totalPages) {
        next = {
          page: parseInt(req.query.page) + 1,
          limit: limit,
        };
      } else {
        return res.status(400).json({ message: `total pages: ${totalPages}` });
      }

      const object = {
        totalPages: totalPages,
        previous: previous,
        next: next,
        professionals: professionals,
      };
      if (professionals) {
        return res.status(200).send(object);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table professionals" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const professional = await Professional.findOne({
        where: { id: req.params.id },
        include: [
          {
            all: true,
          },
        ],
      });
      if (professional) {
        return res.status(200).send(professional);
      } else {
        return res.status(404).json({ message: "Professional not found" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const prof = await Professional.findOne({
        where: { id: req.params.id },
      });
      if (!prof) {
        return res.status(404).json({ message: "Professional not found" });
      }
      try {
        let slug = slugify(data.name, {
          lower: true,
        });
        await prof
          .update({
            name: data.name,
            slug: slug,
            birth: data.birth,
            cpf: data.cpf,
            phone: data.phone,
            email: data.email,
          })
          .then((updateProf) => res.status(200).send(updateProf));
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const prof = await Professional.findOne({
        where: { id: req.params.id },
      });
      if (!prof) {
        return res.status(404).json({ message: "Professional not found" });
      }
      await Professional.destroy({
        where: { id: req.params.id },
      }).then(
        res.status(204).json({ message: "Professional deleted successfully" })
      );
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async restore(req, res) {
    try {
      const prof = await Professional.findOne({
        where: { id: req.params.id },
        paranoid: false,
      });
      if (!prof) {
        return res.status(404).json({ message: "Professional not found" });
      }
      await prof.restore();
      return res.status(200).json({ message: "Restored professional" });
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ProfessionalController();
