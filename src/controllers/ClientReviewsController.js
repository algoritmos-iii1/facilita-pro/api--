const ClientReviews = require("../models/ClientReviews");
const Professional = require("../models/Professional");
const Client = require("../models/Client");
const mail = require("../mail/util/mail.util");

class ClientReviewsController {
  async store(req, res) {
    try {
      const professional_id = req.params.id;
      const data = req.body;

      if (!professional_id || !data.client_id || !data.value) {
        return res.status(400).json({ message: "Missing data" });
      }

      const professional = await Professional.findByPk(professional_id);
      const client = await Client.findByPk(data.client_id);

      if (!professional || !client) {
        return res
          .status(404)
          .json({ message: "Professional or Client not found" });
      }

      await ClientReviews.create({
        client_id: data.client_id,
        professional_id: professional_id,
        value: data.value,
      }).then((newReview) => res.status(201).send(newReview));

      try {
        const subject = `Avaliação enviada para ${professional.name}`;
        const html =
          `Sua avaliação foi enviada com sucesso<br>` +
          `Nota de avaliação: ${data.value}`;

        await mail(client.email, subject, html);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async avg(req, res) {
    try {
      const prof_id = req.params.id;
      const sum = await ClientReviews.sum("value", {
        where: { professional_id: prof_id },
      });
      const count = await ClientReviews.count({
        where: { professional_id: prof_id },
      });
      let avg = parseInt(sum / count);
      return res.status(200).json({ average: avg });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ClientReviewsController();
