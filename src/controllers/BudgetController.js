const Budget = require("../models/Budget");
const Professional = require("../models/Professional");
const mail = require("../mail/util/mail.util");

class BudgetController {
  async store(req, res) {
    try {
      const budget = req.body;

      const professional = await Professional.findByPk(budget.professional_id);

      if (!professional) {
        return res.status(404).json({ message: "Professional not found" });
      }

      let total = (budget.tax * budget.value) / 100 + parseFloat(budget.value);
      try {
        await Budget.create({
          professional_id: budget.professional_id,
          solicitation_id: budget.solicitation_id,
          value: budget.value,
          tax: budget.tax,
          status: budget.status,
          total: total,
        }).then((newBudget) => res.status(201).send(newBudget));
      } catch (err) {
        return res.status(400).json({ message: "Missing data", err });
      }
      try {
        const subject = `<FacilitaPRO> Confirmação de Orçamento para Solicitação ${budget.solicitation_id}`;
        const html =
          `Seu orçamento foi enviado com sucesso!<br>` +
          `Valor total: ${total}`;

        await mail(professional.email, subject, html);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const budgets = await Budget.findAll({
        include: [{ all: true }],
      });
      if (!budgets) {
        return res
          .status(400)
          .json({ message: "There are no records in the table budgets" });
      }
      return res.status(200).send(budgets);
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const budget = await Budget.findOne({
        where: { id: req.params.id },
        include: [{ all: true }],
      });
      if (budget) {
        return res.status(200).send(budget);
      } else {
        return res.status(404).json({ message: "Budget not found" });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const budget = await Budget.findOne({
        where: { id: req.params.id },
      });
      if (!budget) {
        return res.status(404).json({ message: "Budget not found" });
      }
      let total = (data.tax * data.value) / 100 + parseFloat(data.value);
      try {
        await budget
          .update({
            professional_id: data.professional_id,
            solicitation_id: data.solicitation_id,
            value: data.value,
            tax: data.tax,
            status: data.status,
            total: total,
          })
          .then((updateBud) => res.status(200).send(updateBud));
      } catch (err) {
        res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const budget = await Budget.findOne({
        where: { id: req.params.id },
      });
      if (!budget) {
        return res.status(404).json({ message: "Budget not found" });
      }
      budget
        .destroy()
        .then(res.status(204).json({ message: "Budget deleted successfully" }));
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new BudgetController();
