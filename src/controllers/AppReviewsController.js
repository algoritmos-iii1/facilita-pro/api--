const Client = require("../models/Client");
const Professional = require("../models/Professional");
const AppReviews = require("../models/AppReviews");
const mail = require("../mail/util/mail.util");

class AppReviewsController {
  async store(req, res) {
    const { user_id, value } = req.body;

    const client = await Client.findByPk(user_id);
    const professional = await Professional.findByPk(user_id);

    if (!client && !professional) {
      return res.status(404).json({ message: "User not found" });
    }

    if (value < 0 || value == null) {
      return res.status(400).json({ message: "Reviews value is required" });
    }

    await AppReviews.create({
      user_id: user_id,
      value: value,
    }).then((newReview) => res.status(201).send(newReview));

    try {
      const subject = "<FacilitaPRO> Obrigador por avaliar a plataforma";

      if (client) {
        const html = `${client.name}, Obrigado por avaliar a plataforma!`;

        await mail(client.email, subject, html);
      } else {
        const html = `${professional.name}, Obrigado por avaliar a plataforma!`;

        await mail(professional.email, subject, html);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async index(req, res) {
    const appreviews = await AppReviews.findAll();
    const count = await AppReviews.count();
    const sum = await AppReviews.sum("value");
    const avg = parseInt(sum / count);
    if (!appreviews) {
      return res
        .status(404)
        .json({ message: "There are no records in the table appreviews" });
    } else {
      const object = {
        Nota: avg,
        appreviews: appreviews,
      };
      return res.status(200).send(object);
    }
  }
}

module.exports = new AppReviewsController();
