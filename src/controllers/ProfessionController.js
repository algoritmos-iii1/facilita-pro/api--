const Profession = require("../models/Profession");
const Service = require("../models/Service");

class ProfessionController {
  async store(req, res) {
    try {
      const profession = req.body;

      const exists = await Profession.findOne({
        where: { name: profession.name },
      });

      if (exists) {
        return res.status(400).json({ message: "This service already exists" });
      }
      try {
        await Profession.create({
          name: profession.name,
        }).then((newProfession) => res.status(201).json(newProfession));
      } catch (err) {
        return res.status(404).json({ message: "Missing data", err });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const professions = await Profession.findAll({
        include: [
          {
            model: Service,
            as: "services",
          },
        ],
      });
      if (professions) {
        return res.status(200).send(professions);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table professions" });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const profession = await Profession.findOne({
        where: { id: req.params.id },
        include: [
          {
            model: Service,
            as: "services",
          },
        ],
      });
      if (profession) {
        return res.status(200).send(profession);
      } else {
        return res.status(404).json({ message: "Profession not found" });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const profession = await Profession.findOne({
        where: { id: req.params.id },
      });
      if (!profession) {
        return res.status(404).json({ message: "Profession not found" });
      }
      await profession
        .update({
          name: data.name,
        })
        .then((updateProf) => res.status(200).json(updateProf));
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const profession = await Profession.findOne({
        where: { id: req.params.id },
      });
      if (!profession) {
        return res.status(404).json({ message: "Profession not found" });
      }
      await profession
        .destroy()
        .then(
          res.status(204).json({ message: "Profession deleted successfully" })
        );
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ProfessionController();
