const Address = require("../models/Address");
const Client = require("../models/Client");
const Professional = require("../models/Professional");

class AddressController {
  async store(req, res) {
    try {
      const data = req.body;
      const client = await Client.findOne({
        where: { id: data.client_id },
      });
      if (client) {
        try {
          await Address.create({
            client_id: data.client_id,
            street: data.street,
            number: data.number,
            complement: data.complement,
            neighborhood: data.neighborhood,
            city: data.city,
            zipcode: data.zipcode,
          }).then((newAddress) => res.status(201).send(newAddress));
        } catch (err) {
          return res.status(400).json({ message: "Missing data", err });
        }
      }
      const prof = await Professional.findOne({
        where: { id: data.professional_id },
      });
      if (prof) {
        try {
          await Address.create({
            professional_id: data.professional_id,
            street: data.street,
            number: data.number,
            complement: data.complement,
            neighborhood: data.neighborhood,
            city: data.city,
            zipcode: data.zipcode,
          }).then((newAddress) => res.status(201).send(newAddress));
        } catch (err) {
          return res.status(400).json({ message: "Missing data", err });
        }
      }
      return res.status(404).json({ message: "User not found" });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const addresses = await Address.findAll({
        order: [["street", "ASC"]],
        include: [
          {
            all: true,
          },
        ],
      });
      if (!addresses) {
        return res
          .status(400)
          .json({ message: "There are no records in the table addresses" });
      }
      return res.status(200).send(addresses);
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const address = await Address.findOne({
        where: { id: req.params.id },
        include: [
          {
            all: true,
          },
        ],
      });
      if (!address) {
        return res.status(400).json({ message: "Address not found" });
      }
      return res.status(200).send(address);
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const address = await Address.findOne({
        where: { id: req.params.id },
      });
      if (!address) {
        return res.status(400).json({ message: "Address not found" });
      }
      try {
        await address
          .update({
            street: data.street,
            number: data.number,
            complement: data.complement,
            neighborhood: data.neighborhood,
            city: data.city,
            zipcode: data.zipcode,
          })
          .then((updateAddress) => res.status(200).send(updateAddress));
      } catch (err) {
        return res.status(400).json({ message: "Missing data", err });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const address = await Address.findOne({
        where: { id: req.params.id },
      });
      if (!address) {
        return res.status(400).json({ message: "Address not found" });
      }
      await address
        .destroy()
        .then(
          res.status(204).json({ message: "Address deleted successfully" })
        );
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new AddressController();
