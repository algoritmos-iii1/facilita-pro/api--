const Service = require("../models/Service");

class ServiceController {
  async store(req, res) {
    try {
      const service = req.body;
      const exists = await Service.findOne({
        where: { name: service.name },
      });
      if (exists) {
        return res.status(400).json({ message: "This service already exists" });
      }
      await Service.create({
        profession_id: service.profession_id,
        name: service.name,
        description: service.description,
      }).then((newService) => res.status(201).send(newService));
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const services = await Service.findAll();
      if (services) {
        return res.status(200).send(services);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table services" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async withParanoid(req, res) {
    try {
      const services = await Service.findAll({
        paranoid: false,
      });
      if (services) {
        return res.status(200).send(services);
      } else {
        return res
          .status(404)
          .send({ message: "There are no records in the table services" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const service = await Service.findOne({
        where: { id: req.params.id },
      });
      if (service) {
        return res.status(200).send(service);
      } else {
        return res.status(404).json({ message: "Service not found" });
      }
    } catch (err) {
      res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const service = await Service.findOne({
        where: { id: req.params.id },
      });

      if (!service) {
        return res.status(404).json({ message: "Service not found" });
      }

      await service
        .update({
          profession_id: data.profession_id,
          name: data.name,
          description: data.description,
        })
        .then((updateService) => res.status(200).send(updateService));
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const service = await Service.findByPk(req.params.id);
      if (!service) {
        return res.status(404).json({ message: "Service not found" });
      }
      await Service.destroy({
        where: { id: req.params.id },
      }).then(
        res.status(204).json({ message: "Service deleted successfully" })
      );
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async restore(req, res) {
    try {
      const service = await Service.findOne({
        where: { id: req.params.id },
        paranoid: false,
      });
      if (!service) {
        return res.status(404).json({ message: "Service not found" });
      }
      await service.restore();
      return res.status(200).json({ message: "Restored service" });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new ServiceController();
