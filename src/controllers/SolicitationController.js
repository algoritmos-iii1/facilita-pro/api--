const Solicitation = require("../models/Solicitation");
const Client = require("../models/Client");
const Address = require("../models/Address");
const Service = require("../models/Service");
const Budget = require("../models/Budget");
const mail = require("../mail/util/mail.util");

class SolicitationController {
  async store(req, res) {
    try {
      const {
        client_id,
        address_id,
        service_id,
        rated = false,
        ...solicitation
      } = req.body;
      const { location: url = "" } = req.file;

      const client = await Client.findByPk(client_id);

      if (!client) {
        return res.status(401).json({ message: "Client not found" });
      }

      const address = await Address.findByPk(address_id);

      if (!address) {
        return res.status(401).json({ message: "Address not found" });
      }

      const service = await Service.findByPk(service_id);

      if (!service) {
        return res.status(401).json({ message: "Service not found" });
      }

      try {
        await Solicitation.create({
          client_id: client_id,
          service_id: service_id,
          address_id: address_id,
          initialDate: solicitation.initialDate,
          description: solicitation.description,
          finallyDate: solicitation.finallyDate,
          status: solicitation.status,
          rated: rated,
          photoUrl: url,
        }).then((newSolicitation) => res.status(201).send(newSolicitation));
      } catch (err) {
        return res.status(400).json({ message: "Missing data", err });
      }

      try {
        const subject = "<FacilitaPRO> Confirmação de Solicitação";
        const html =
          `Sua solicitação foi enviada com sucesso!<br>` +
          `Serviço desejado: ${service.name}<br> Data inicial: ${solicitation.initialDate}<br>` +
          `Data Final: ${solicitation.finallyDate}<br> Endereço: ${address.street}, ${address.number} - ${address.city}<br>` +
          `Descrição do serviço: ${solicitation.description}`;

        await mail(client.email, subject, html);
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async index(req, res) {
    try {
      const solicitations = await Solicitation.findAll({
        include: [
          {
            all: true,
          },
        ],
      });
      if (!solicitations) {
        return res
          .status(400)
          .json({ message: "There are no records in the table solicitations" });
      }
      return res.status(200).send(solicitations);
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async show(req, res) {
    try {
      const solicitation = await Solicitation.findOne({
        where: { id: req.params.id },
        include: [{ all: true }],
      });
      if (solicitation) {
        return res.status(200).send(solicitation);
      } else {
        return res.status(404).json({ message: "Solicitation not found" });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async update(req, res) {
    try {
      const data = req.body;
      const solicitation = await Solicitation.findOne({
        where: { id: req.params.id },
      });

      if (!solicitation) {
        return res.status(404).json({ message: "Solicitation not found" });
      }

      await solicitation
        .update({
          client_id: data.client_id,
          service_id: data.service_id,
          initialDate: data.initialDate,
          description: data.description,
          finallyDate: data.finallyDate,
          status: data.status,
          rated: data.rated,
        })
        .then((updateSol) => res.status(200).send(updateSol));
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async delete(req, res) {
    try {
      const solicitation = await Solicitation.findOne({
        where: { id: req.params.id },
      });
      if (!solicitation) {
        return res.status(404).json({ message: "Solicitation not found" });
      }
      solicitation
        .destroy()
        .then(
          res.status(204).json({ message: "Solicitation deleted successfully" })
        );
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }
}

module.exports = new SolicitationController();
