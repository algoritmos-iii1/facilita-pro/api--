const router = require('express').Router()
const MailController = require('./MailController')
const adminMiddleware = require('../auth/adminAuth')

router.post('/mail', adminMiddleware, MailController.mail)
router.post('/mail/clients', adminMiddleware, MailController.clientsMail)
router.post('/mail/professionals', adminMiddleware, MailController.professionalsMail)

module.exports = router;