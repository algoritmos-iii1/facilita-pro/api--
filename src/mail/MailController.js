const nodemailer = require("nodemailer");
const Client = require("../models/Client");
const Professional = require("../models/Professional");

class MailController {
  async mail(req, res) {
    try {
      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        auth: {
          user: "facilitapro.contact@gmail.com",
          pass: "hBv5gEr21kadNA",
        },
      });

      const { to, subject, html } = req.body;

      if (!to || !subject || !html) {
        return res.status(400).json({ message: "Missing data" });
      }

      transporter
        .sendMail({
          from: "FacilitaPRO <facilitapro.contact@gmail.com>",
          to: to,
          subject: subject,
          html: html,
        })
        .then(() => {
          return res.status(200).json({ message: "Email enviado" });
        })
        .catch((err) => {
          return res.status(400).send(err);
        });
    } catch (err) {
      return res.status(500).json({ message: "Internal server error", err });
    }
  }

  async clientsMail(req, res) {
    const clients = await Client.findAll();

    if (!clients) {
      return res.status(404).json({ message: "No clients" });
    }

    let emails = [];

    clients.map((key) => emails.push(key.email));

    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      auth: {
        user: "facilitapro.contact@gmail.com",
        pass: "hBv5gEr21kadNA",
      },
    });

    const { subject, html } = req.body;

    if (!subject || !html) {
      return res.status(400).json({ message: "Missing data" });
    }

    transporter
      .sendMail({
        from: "FacilitaPRO <facilitapro.contact@gmail.com>",
        to: emails,
        subject: subject,
        html: html,
      })
      .then(() => {
        return res.status(200).json({ message: "Email enviado" });
      })
      .catch((err) => {
        return res.status(400).send(err);
      });
  }

  async professionalsMail(req, res) {
    const professionals = await Professional.findAll();

    let emails = [];

    if (!professionals) {
      return res.status(404).json({ message: "No professinals" });
    }

    professionals.map((key) => emails.push(key.email));

    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      auth: {
        user: "facilitapro.contact@gmail.com",
        pass: "hBv5gEr21kadNA",
      },
    });

    const { subject, html } = req.body;

    if (!subject || !html) {
      return res.status(400).json({ message: "Missing data" });
    }

    transporter
      .sendMail({
        from: "FacilitaPRO <facilitapro.contact@gmail.com>",
        to: emails,
        subject: subject,
        html: html,
      })
      .then(() => {
        return res.status(200).json({ message: "Email enviado" });
      })
      .catch((err) => {
        return res.status(400).send(err);
      });
  }
}

module.exports = new MailController();
