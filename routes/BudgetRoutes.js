const router = require('express').Router()
const BudgetController = require('../src/controllers/BudgetController')
const authMiddleware = require('../src/auth/auth')

router.post('/', authMiddleware, BudgetController.store)
router.put('/:id', authMiddleware, BudgetController.update)
router.get('/', authMiddleware, BudgetController.index)
router.get('/:id', authMiddleware, BudgetController.show)
router.delete('/:id', authMiddleware, BudgetController.delete)

module.exports = router;