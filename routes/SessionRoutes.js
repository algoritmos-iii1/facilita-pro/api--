const router = require('express').Router()
const SessionController = require('../src/auth/SessionController')

router.get('/login', SessionController.login)

module.exports = router;