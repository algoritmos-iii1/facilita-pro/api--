const router = require('express').Router()
const AdminController = require('../src/controllers/AdminController')
const adminMiddleware = require('../src/auth/adminAuth')

router.post('/', adminMiddleware, AdminController.store)
router.put('/:id', adminMiddleware, AdminController.update)
router.get('/', adminMiddleware, AdminController.index)
router.get('/:id', adminMiddleware, AdminController.show)
router.delete('/:id', adminMiddleware, AdminController.delete)
router.get('/restore/:id', adminMiddleware, AdminController.restore)

module.exports = router;