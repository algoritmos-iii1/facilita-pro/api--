const router = require("express").Router();
const ProfessionalController = require("../src/controllers/ProfessionalController");
const authMiddleware = require("../src/auth/auth");
const adminMiddleware = require("../src/auth/adminAuth");
const multer = require("multer");
const multerConfig = require("../src/config/multer");

router.post(
  "/",
  multer(multerConfig).single("file"),
  adminMiddleware,
  ProfessionalController.store
);
router.put("/:id", adminMiddleware, ProfessionalController.update);
router.get("/", authMiddleware, ProfessionalController.index);
router.get("/paginate", authMiddleware, ProfessionalController.paginate);
router.get("/:id", authMiddleware, ProfessionalController.show);
router.delete("/:id", adminMiddleware, ProfessionalController.delete);
router.get("/restore/:id", adminMiddleware, ProfessionalController.restore);

module.exports = router;
