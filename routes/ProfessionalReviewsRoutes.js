const router = require('express').Router()
const ProfessionalReviewsController = require('../src/controllers/ProfessionalReviewsController')
const authMiddleware = require('../src/auth/auth')

router.post('/professional/reviews/:id', authMiddleware, ProfessionalReviewsController.store)
router.get('/avg/client/:id', authMiddleware, ProfessionalReviewsController.avg)

module.exports = router;
